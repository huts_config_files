#PS1='\[\e[00;36m\]atom:\[\e[00;32m\]\w\[\e[01;30m\]$ \[\e[00;00m\]'
source /home/hut/color.sh

if [ "$PATH_WAS_SET" = "" ]; then
	export PATH="/home/hut/bin:"$PATH
fi
source /etc/bash_completion.d/*

export EDITOR="vim"

PATH_WAS_SET=1

LSFLAGS=' --color --group-directories-first'

alias fm='cd "`ranger --cd 3>&1 1>&2`"'
alias dr='screen -dRRS mainsession'
alias ddr='screen -xRRS atommain'
alias rr='source ~/.bashrc'
alias checkout='git checkout'
alias commit='git citool'

alias ll='ls -l'$LSFLAGS
alias ls='ls'$LSFLAGS
alias '..'='cd ..'
alias '...'='cd ../..'
alias '....'='cd ../../..'
alias '.....'='cd ../../../..'
alias '......'='cd ../../../../..'

alias reboot='sudo reboot'
alias shutdown='sudo shutdown -h now'
alias suvi='sudo vi'

case "$TERM" in
xterm*|rxvt*)
	PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1";;
esac

alias m="mplayer -msglevel all=0"
